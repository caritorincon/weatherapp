package com.example.caritorincon.weatherapp.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.caritorincon.weatherapp.R;
import com.example.caritorincon.weatherapp.adapter.WeatherDayAdapter;
import com.example.caritorincon.weatherapp.fragment.WeatherFragment;
import com.example.caritorincon.weatherapp.model.CityModel;
import com.example.caritorincon.weatherapp.model.CityWeatherModel;
import com.example.caritorincon.weatherapp.model.pojos.WeatherCity;

import static com.example.caritorincon.weatherapp.model.pojos.WeatherCity.DAY_WEATHER_DETAIL;

public class ChooseCityActivity extends AppCompatActivity {

    private static final String TAG = "ChooseCityActivity";
    private CityModel.City selectedCity;
    private ProgressDialog pDialog;
    private WeatherFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_city);

        loadFragment();
        loadCities();

        pDialog = new ProgressDialog(this);
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setMessage("Loading City Weather...");
        pDialog.setCancelable(true);
    }

    public void goOnClick(View v) {
        Log.i(TAG, "Finding info of " + selectedCity);
        if (isNetworkAvailable()) {
            loadWeatherDays();
        } else {
            Toast.makeText(this, "There is not Internet Connection", Toast.LENGTH_LONG).show();
        }

    }

    private void loadFragment() {
        Log.i(TAG, "Loading Fragment");

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        fragment = WeatherFragment.builderDay(CityWeatherModel.weatherCityList, new WeatherDayAdapter.WeatherCityItemListener() {
            @Override
            public void onProductClick(WeatherCity item) {
                showWeatherDetail(item);
            }
        });
        transaction.add(R.id.fragment_weather_list, fragment);
        transaction.commit();
    }


    private void loadWeatherDays() {
        Log.i(TAG, "Start loadWeatherDays");
        SearchWeatherCityAsyncTask searchWeatherCityAsyncTask = new SearchWeatherCityAsyncTask();
        searchWeatherCityAsyncTask.execute(selectedCity.getId());
        Log.i(TAG, "End loadWeatherDays");
    }

    private void loadCities() {
        // use adapter to populate the spinner
        ArrayAdapter<CityModel.City> cityAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, CityModel.getCities());
        // this is just for spinners
        cityAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        //set the adapter to the spinner
        Spinner citySpinner = (Spinner) findViewById(R.id.citySpinner);
        citySpinner.setAdapter(cityAdapter);

        //add the event

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCity = CityModel.getCities()[position];
                CityWeatherModel.clearWeatherCity();
                fragment.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectedCity = CityModel.getCities()[0];
            }
        });

    }

    public void showWeatherDetail(WeatherCity weatherCity) {
        Log.d(TAG, "[showWeatherDetail] Selected item " + weatherCity.getDayWeather().getDate());
        Intent intent = new Intent(this, WeatherDetailActivity.class);
        intent.putExtra(DAY_WEATHER_DETAIL, weatherCity);

        startActivity(intent);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.isConnected()) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onStop()");
        super.onResume();

    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();

    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();
        Log.d(TAG, "after onStop()");

    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart()");
        super.onRestart();

    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onRestart()");
        super.onDestroy();
    }

    private class SearchWeatherCityAsyncTask extends AsyncTask<String, Integer, Void> {

        @Override
        protected Void doInBackground(String... params) {
            if (params != null && params.length > 0) {
                try {
                    CityWeatherModel.loadWeatherCity(params[0]);
                } catch (Exception e) {
                    Log.e(TAG, "Error in loadWeatherCity", e);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {

            pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    SearchWeatherCityAsyncTask.this.cancel(true);
                }
            });
            pDialog.setProgress(0);
            pDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            fragment.notifyDataSetChanged();
            pDialog.dismiss();
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(ChooseCityActivity.this, "The task was cancelled!", Toast.LENGTH_SHORT).show();
        }

    }
}
