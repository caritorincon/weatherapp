package com.example.caritorincon.weatherapp.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.caritorincon.weatherapp.R;
import com.example.caritorincon.weatherapp.adapter.WeatherAdapter;
import com.example.caritorincon.weatherapp.adapter.WeatherDayAdapter;
import com.example.caritorincon.weatherapp.adapter.WeatherDetailAdapter;
import com.example.caritorincon.weatherapp.model.pojos.Weather;
import com.example.caritorincon.weatherapp.model.pojos.WeatherCity;

import java.io.Serializable;
import java.util.List;


/**
 * Fragment that contains the recycler view
 */
public class WeatherFragment extends Fragment {

    private static final String TYPE = "type";
    private static final String LIST = "list";
    private static final String LISTENER = "listener";

    private static final int DAY_ADAPTER = 1;
    private static final int DETAIL_ADAPTER = 2;


    private String tag = "WeatherFragment";
    private WeatherAdapter weatherAdapter;


    public WeatherFragment() {
        super();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * For showing
     *
     * @return A new instance of fragment WeatherFragment.
     */
    public static WeatherFragment builderDay(List<WeatherCity> list, WeatherDayAdapter.WeatherCityItemListener listener) {
        WeatherFragment fragment = new WeatherFragment();

        Bundle args = new Bundle();
        args.putInt(TYPE, DAY_ADAPTER);
        args.putSerializable(LIST, (Serializable) list);
        args.putSerializable(LISTENER, listener);

        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * For showing
     *
     * @return A new instance of fragment WeatherFragment.
     */
    public static WeatherFragment builderDetail(List<Weather> list) {
        WeatherFragment fragment = new WeatherFragment();

        Bundle args = new Bundle();
        args.putInt(TYPE, DETAIL_ADAPTER);
        args.putSerializable(LIST, (Serializable) list);

        fragment.setArguments(args);
        return fragment;
    }

    public void notifyDataSetChanged() {
        weatherAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        if (getArguments() != null && getArguments().containsKey(TYPE) && getArguments().containsKey(LIST)) {
            if (getArguments().getInt(TYPE) == DAY_ADAPTER) {
                tag += "[Day]";
                configRecyclerViewDay(view, (List<WeatherCity>) getArguments().get(LIST), (WeatherDayAdapter.WeatherCityItemListener) getArguments().get(LISTENER));
            } else {
                tag += "[Det]";
                configRecyclerViewDetail(view, (List<Weather>) getArguments().get(LIST));
            }

        } else {
            Toast.makeText(view.getContext(), "No se pudo cargar la lista", Toast.LENGTH_LONG).show();
        }

        return view;
    }

    private void configRecyclerViewDay(View v, List<WeatherCity> list, WeatherDayAdapter.WeatherCityItemListener listener) {
        Log.i(tag, "Start configRecyclerViewDay");

        RecyclerView weatherRecyclerView = (RecyclerView) v.findViewById(R.id.weatherLRecyclerView);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false));
        weatherRecyclerView.setHasFixedSize(true);

        weatherAdapter = new WeatherDayAdapter(list, listener);
        weatherRecyclerView.setAdapter(weatherAdapter);
    }

    private void configRecyclerViewDetail(View v, List<Weather> list) {
        Log.i(tag, "Start configRecyclerViewDetail");

        RecyclerView weatherRecyclerView = (RecyclerView) v.findViewById(R.id.weatherLRecyclerView);
        weatherRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false));
        weatherRecyclerView.setHasFixedSize(true);
        weatherAdapter = new WeatherDetailAdapter(list);
        weatherRecyclerView.setAdapter(weatherAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(tag, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(tag, "onPause");
    }
}
