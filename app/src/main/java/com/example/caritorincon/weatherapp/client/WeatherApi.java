package com.example.caritorincon.weatherapp.client;

import com.example.caritorincon.weatherapp.client.pojos.CityWeather;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by carito on 10/21/2016.
 * Interface that represents the methods we will use from de Weather API
 */

public interface WeatherApi {

    @GET("/data/2.5/forecast")
    CityWeather getCityWeatherNextDays(@Query("id") String id,
                                       @Query("appid") String key);
}
