package com.example.caritorincon.weatherapp.client.pojos;

/**
 * City Pojo
 * Created by carito on 10/21/2016.
 */

public class City {

    private static final String TO_STRING = "[id :  %d , name : %s , country : %s , population :  %d] ";

    private Integer id;
    private String name;
    private String country;
    private Integer population;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return String.format(TO_STRING, id, name, country, population);
    }
}
