package com.example.caritorincon.weatherapp.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.caritorincon.weatherapp.R;
import com.example.caritorincon.weatherapp.fragment.WeatherFragment;
import com.example.caritorincon.weatherapp.model.pojos.Weather;
import com.example.caritorincon.weatherapp.model.pojos.WeatherCity;

import java.util.List;

import static com.example.caritorincon.weatherapp.model.pojos.WeatherCity.DAY_WEATHER_DETAIL;

public class WeatherDetailActivity extends AppCompatActivity {

    public static final String TAG = "WeatherDetailActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_detail);
        showDetail();
    }

    public void showDetail() {
        if (getIntent().hasExtra(DAY_WEATHER_DETAIL)) {
            WeatherCity detail = (WeatherCity) getIntent().getSerializableExtra(DAY_WEATHER_DETAIL);
            loadFragment(detail.getDetailWeatherList());
        }
    }

    private void loadFragment(List<Weather> list) {
        Log.i(TAG, "Loading Fragment");
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        // transaction.add(R.id.activity_weather_detail, fragment);
        transaction.replace(R.id.activity_weather_detail, WeatherFragment.builderDetail(list));
        transaction.commit();
    }
}
