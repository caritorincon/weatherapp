package com.example.caritorincon.weatherapp.model;
/**
 * Created by carito on 10/11/2016.
 * City model . it load the cities with the id
 */

public class CityModel {

    private static final City[] cities = {new City("1851632", "Shuzenji"), new City("3688689", "Bogota"), new City("3687925", "Cali"), new City("3674962",
            "Medellin")};


    public static City[] getCities() {
        return cities;
    }

    public static class City {
        private String name;
        private String id;

        City(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString(){
            return this.name ;
        }
    }
}
