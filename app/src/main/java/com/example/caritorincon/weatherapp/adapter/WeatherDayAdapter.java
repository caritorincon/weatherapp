package com.example.caritorincon.weatherapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.caritorincon.weatherapp.R;
import com.example.caritorincon.weatherapp.model.CityWeatherModel;
import com.example.caritorincon.weatherapp.model.pojos.WeatherCity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by carito on 10/11/2016.
 * WeatherAdapter using RecyclerView
 */

public class WeatherDayAdapter extends WeatherAdapter {
    private List<WeatherCity> weatherCityList;
    private WeatherCityItemListener listener;

    public WeatherDayAdapter(List<WeatherCity> weatherCityList, WeatherCityItemListener listener) {
        this.listener = listener;
        this.weatherCityList = weatherCityList;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
        return new WeatherDayViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        ((WeatherDayViewHolder) holder).bindWeatherCity(weatherCityList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherCityList.size();
    }

    private WeatherCity getItem(int position) {
        return weatherCityList.get(position);
    }

    public interface WeatherCityItemListener extends Serializable {
        void onProductClick(WeatherCity clickedNote);
    }

    class WeatherDayViewHolder extends WeatherViewHolder implements View.OnClickListener {

        private WeatherCityItemListener mItemListener;

        WeatherDayViewHolder(View itemView, WeatherCityItemListener listener) {
            super(itemView);
            mItemListener = listener;
            itemView.setOnClickListener(this);
        }

        void bindWeatherCity(WeatherCity weatherCity) {
            mainTemp.setText(weatherCity.getDayWeather().getTemp());
            date.setText(weatherCity.getDayWeather().getDate());
            mainWeather.setText(weatherCity.getDayWeather().getMainWeather());
            icon.setImageBitmap(CityWeatherModel.getIcon(weatherCity.getDayWeather().getIconPath()));
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            WeatherCity product = getItem(position);
            mItemListener.onProductClick(product);
        }
    }


}
