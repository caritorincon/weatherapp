package com.example.caritorincon.weatherapp.model.pojos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by carito on 10/26/2016.
 */

public class Weather implements Serializable {

    private static final double KTC = 273.15;
    private static final String GRADE = "º";
    private static final String TO_STRING = "[temp :  %d , date : %s , mainWeather : %s , iconPath  :  %s] ";
    private final String hourPattern = "hh:mm aaa";
    private final String dayPattern = "MMMM d";
    private final SimpleDateFormat sdfHour = new SimpleDateFormat(hourPattern);
    private final SimpleDateFormat sdfDay = new SimpleDateFormat(dayPattern);
    private Long temp;
    private Date date;
    private String mainWeather;
    private String iconPath;

    public Weather(Date date, String iconPath, String mainWeather, Double temp) {
        this.date = date;
        this.iconPath = iconPath;
        this.mainWeather = mainWeather;
        this.temp = Math.round(temp - KTC);
    }

    public String getDate() {
        return sdfDay.format(this.date);
    }

    public String getHourDate() {
        return sdfHour.format(this.date);
    }

    public String getMainWeather() {
        return mainWeather;
    }

    public String getTemp() {
        return temp.toString() + GRADE;
    }

    public String getIconPath() {
        return iconPath;
    }

    @Override
    public String toString() {
        return String.format(TO_STRING, temp, getHourDate(), mainWeather, iconPath);
    }
}
