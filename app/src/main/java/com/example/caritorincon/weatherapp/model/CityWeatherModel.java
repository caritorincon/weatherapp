package com.example.caritorincon.weatherapp.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.caritorincon.weatherapp.client.RestClient;
import com.example.caritorincon.weatherapp.client.WeatherApi;
import com.example.caritorincon.weatherapp.client.pojos.CityWeather;
import com.example.caritorincon.weatherapp.model.pojos.Weather;
import com.example.caritorincon.weatherapp.model.pojos.WeatherCity;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carito on 10/11/2016.
 * CityWeatherModel
 */

public class CityWeatherModel {

    public static final List<WeatherCity> weatherCityList = new ArrayList<>();
    private static final String TAG = "CityWeatherModel";
    // TODO where can i put this????
    private static final String KEY = "719656b90692bc62c73a0c13bb3a8da8";
    private static final String ST_URL = "http://openweathermap.org/img/w/%s.png";
    private static Map<String, Bitmap> iconCache = new HashMap<>();

    public static void loadWeatherCity(String id) throws Exception {
        Log.i(TAG, "searching weather for id city : " + id);
        Log.i(TAG, "Start Retrofit Client");

        WeatherApi client = RestClient.createService(WeatherApi.class);
        CityWeather cityWeather;

        weatherCityList.clear();

        try {

            cityWeather = client.getCityWeatherNextDays(id, KEY);
            int lastIndex = -1;

            for (com.example.caritorincon.weatherapp.client.pojos.List detatil : cityWeather.getList()) {

                Date date = new Date((long) detatil.getDt() * 1000);
                Weather weather = new Weather(date, detatil.getWeather().get(0).getIcon(), detatil.getWeather().get(0).getMain(), detatil.getMain().getTemp());
                addIcon(detatil.getWeather().get(0).getIcon());

                if (lastIndex != -1 && weatherCityList.get(lastIndex).getDayWeather().getDate().equals(weather.getDate())) {
                    weatherCityList.get(lastIndex).addToDetailWeatherList(weather);
                } else {
                    WeatherCity newDay = new WeatherCity(weather);
                    newDay.addToDetailWeatherList(weather);
                    weatherCityList.add(newDay);
                    lastIndex++;
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Error loading weather info", e);
            throw new Exception(e);
        }
    }

    public static void clearWeatherCity() {
        weatherCityList.clear();
    }

    public static Bitmap getIcon(String path) {
        return iconCache.get(path);
    }

    private static void addIcon(String iconPath) {

        if (!iconCache.containsKey(iconPath)) {
            {
                try {
                    URL url = new URL(String.format(ST_URL, iconPath));
                    InputStream is = new BufferedInputStream(url.openStream());
                    iconCache.put(iconPath, BitmapFactory.decodeStream(is));
                } catch (Exception e) {
                    Log.e(TAG, "Error loading icon", e);
                }
            }

        }
    }
}
