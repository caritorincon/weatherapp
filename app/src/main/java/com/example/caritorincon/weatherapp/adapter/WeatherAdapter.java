package com.example.caritorincon.weatherapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.caritorincon.weatherapp.R;

/**
 * Created by carito on 10/11/2016.
 * WeatherAdapter using RecyclerView
 */

public abstract class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {


    public abstract class WeatherViewHolder extends RecyclerView.ViewHolder {
        //private ImageView weatherImg ;
        protected TextView mainTemp;
        protected TextView date;
        protected TextView mainWeather;
        protected ImageView icon;


        WeatherViewHolder(View itemView) {
            super(itemView);
            mainTemp = (TextView) itemView.findViewById(R.id.main_temp);
            date = (TextView) itemView.findViewById(R.id.date);
            mainWeather = (TextView) itemView.findViewById(R.id.main_weather);
            icon = (ImageView) itemView.findViewById(R.id.weather_img);
        }
    }

}
