package com.example.caritorincon.weatherapp.client;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by carito on 10/20/2016.
 * Client
 */

public class RestClient {
    //TODO I dont know if this property goes here .. check!!!
    public static final String API_BASE_URL = "http://api.openweathermap.org";

    private static RestAdapter.Builder builder = new RestAdapter.Builder()
            .setEndpoint(API_BASE_URL)
            .setClient(new OkClient(new OkHttpClient()))
            .setLogLevel(RestAdapter.LogLevel.BASIC);

    public static <S> S createService(Class<S> serviceClass) {
        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }
}
