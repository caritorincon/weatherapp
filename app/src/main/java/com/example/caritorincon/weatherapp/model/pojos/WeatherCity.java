package com.example.caritorincon.weatherapp.model.pojos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Class that contains the first day weather and a list of Weather , that list is the detail weather each  3 hour
 * Created by carito on 10/26/2016.
 */

public class WeatherCity implements Serializable {

    public static final String DAY_WEATHER_DETAIL = "1";
    private Weather dayWeather;
    private List<Weather> detailWeatherList = new ArrayList<>();

    public WeatherCity(Weather dayWeather) {
        this.dayWeather = dayWeather;
    }

    public Weather getDayWeather() {
        return dayWeather;
    }

    public void addToDetailWeatherList(Weather w) {
        detailWeatherList.add(w);
    }

    public List<Weather> getDetailWeatherList() {
        return detailWeatherList;
    }

}
