package com.example.caritorincon.weatherapp.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.caritorincon.weatherapp.R;
import com.example.caritorincon.weatherapp.model.CityWeatherModel;
import com.example.caritorincon.weatherapp.model.pojos.Weather;

import java.util.List;

/**
 * Created by carito on 10/11/2016.
 * WeatherAdapter using RecyclerView
 */

public class WeatherDetailAdapter extends WeatherAdapter {
    private List<Weather> weatherCityList;

    public WeatherDetailAdapter(List<Weather> weatherCityList) {
        Log.d("WeatherDetailAdapter", "size " + weatherCityList.size());
        for (Weather w : weatherCityList) {
            Log.d("WeatherDetailAdapter", " > data " + w.toString());
        }

        this.weatherCityList = weatherCityList;
    }

    @Override
    public WeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_item, parent, false);
        return new WeatherDetailViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WeatherViewHolder holder, int position) {
        Log.d("WeatherDetailAdapter", "pos " + position);
        Log.d("WeatherDetailAdapter", "data " + weatherCityList.get(position));

        ((WeatherDetailViewHolder) holder).bindWeatherCity(weatherCityList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherCityList.size();
    }

    class WeatherDetailViewHolder extends WeatherViewHolder {

        WeatherDetailViewHolder(View itemView) {
            super(itemView);
        }

        void bindWeatherCity(Weather weather) {
            mainTemp.setText(weather.getTemp());
            date.setText(weather.getHourDate());
            mainWeather.setText(weather.getMainWeather());
            icon.setImageBitmap(CityWeatherModel.getIcon(weather.getIconPath()));
        }
    }


}
